# {template-title}

{template-description}.

## Setup

Common setup to run a single ansible playbook, for anything more use Google.

```sh
sudo mkdir -p /etc/ansible
echo -e "[local]\nlocalhost ansible_connection=local" | \
  sudo tee /etc/ansible/hosts
```

## Running

```sh
git clone {template-url}
ansible-playbook --ask-become-pass {template}.yml
```
